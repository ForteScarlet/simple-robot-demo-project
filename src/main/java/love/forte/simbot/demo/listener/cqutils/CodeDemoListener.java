package love.forte.simbot.demo.listener.cqutils;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.anno.depend.Beans;
import com.forte.qqrobot.beans.messages.msgget.GroupMsg;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.beans.types.KeywordMatchType;
import com.forte.qqrobot.sender.MsgSender;
import com.simplerobot.modules.utils.CodeTemplate;
import com.simplerobot.modules.utils.KQCode;
import com.simplerobot.modules.utils.KQCodeUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 此类展示如何使用 {@link com.simplerobot.modules.utils.KQCodeUtils} 发送一些特殊消息，例如图片、at信息等。
 *
 * 此类的监听器中，以"图片"为例。
 *
 *
 *
 * @author <a href="https://github.com/ForteScarlet"> ForteScarlet </a>
 */
@Beans
public class CodeDemoListener {

    /**
     * 示例用的随便找的一个图片url。
     */
    private static final String DEMO_IMG_URL = "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1906469856,4113625838&fm=26&gp=0.jpg";


    /**
     * 随便一个群聊中，发送一句 “图片” 并at bot, 他就会发送一张图片。
     *
     * 值得一提的是，mirai中，截止到我写下这段文字的时间（2020/9/27）,他发送“私聊图片”的稳定性远不如发送“群聊图片”，
     * 因此如果你尝试发送私聊图片但是发现发送出来的图片都是裂了的或者无法查看的，这属于正常现象。
     */
    @Listen(MsgGetTypes.groupMsg)
    // 消息过滤为 "图片", bot的at检测为true, 匹配规则为移除了CQ码后直接equals.
    @Filter(value = "图片", at = true, keywordMatchType = KeywordMatchType.RE_CQCODE_EQUALS)
    public void inGroupForImg(GroupMsg groupMsg, MsgSender msgSender) {
        // 得到一个KQCodeUtils实例。
        final KQCodeUtils utils = KQCodeUtils.INSTANCE;

        // 发送图片为发送image类型的CQ码, 参数一般为file=图片路径。
        // 例如：[CQ:image,file=1.jpg]

        // 方法1: 手动填入参数 - 字符串参数
        final String imageCode1 = utils.toCq("image", "file=" + DEMO_IMG_URL);

        // 方法2: 手动填入参数 - map参数
        final Map<String, String> paramMap = new HashMap<>(1);
        paramMap.put("file", DEMO_IMG_URL);
        final String imageCode2 = utils.toCq("image", paramMap);

        // 方法3: 手动填入参数 - 字符串参数, 得到KQCode实例并toString
        final KQCode imageCode3Kq = utils.toKq("image", "file=" + DEMO_IMG_URL);
        final String imageCode3 = imageCode3Kq.toString();

        // 但是上面这些方法对于一个具有固定格式的"image"类型的cq码来说，略显繁琐，也存在一定的出错几率。
        // 因此KQCodeUtils中提供了模板方法，来支持获取一些常见格式的码。


        // 得到返回值类型为String类型的code模板。
        // 如果使用utils.getKqCodeTemplate(), 那么你得到的返回值类型是CodeTemplate<KQCode>, 即模板的返回值为KQCode。
        // 这里没有必要, 因此直接使用字符串模板即可。
        final CodeTemplate<String> stringTemplate = utils.getStringTemplate();

        // 方法4: 通过模板获取
        String imageCode4 = stringTemplate.image(DEMO_IMG_URL);

        // 发送, 发送到当前消息的群里。
        msgSender.SENDER.sendGroupMsg(groupMsg, imageCode4);
    }




}
